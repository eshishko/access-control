#include <SPI.h>
#include <MFRC522.h>
#include <Ethernet.h>
#include <Servo.h>

//#define SD_PIN          4          // SD card pin
#define RST_PIN         8          
#define SS_PIN          9          // RC522 pin
#define GREEN_PIN       5          // GREEN LIGHT PIN
#define RED_PIN         6          // RED LIGHT PIN
#define SERVO_PIN       3          // SERVO MOTOR PIN

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.
EthernetClient client;
Servo turnstile;

MFRC522::MIFARE_Key key;

char server[] = "www.grep.lv";    // name address (using DNS)
byte mac[]    = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
String uids[4];

String readerUid           = "abc-123";
String controllerUid       = "cd-24";
bool successPing           = false;     // check ping response, if no valid response, compare only hardcoded uids
unsigned long lastPingTime = 0;         // last time ping, in milliseconds

void setup() {
  pinMode(4, INPUT_PULLUP);     // button
  pinMode(RED_PIN, OUTPUT);     // red light - no internet
  pinMode(GREEN_PIN, OUTPUT);   // green light - access
  turnstile.attach(SERVO_PIN);  // servo motor
  
  Ethernet.init(10);
    
  Serial.begin(9600); // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  SPI.begin();        // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card

  // Prepare the key (used both as key A and as key B)
  // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }

  Serial.println(F("Scan a MIFARE Classic."));
  Serial.print(F("Using key B:"));
  dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
  Serial.println();
 
  //set up allowed uids 
  uids[0] = "66938d1a";
  uids[1] = "767e4f1a";

  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
      }
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // try to congifure using IP address instead of DHCP:
    //Ethernet.begin(mac, ip, myDns);
    Serial.print("Ethernet IP address: ");
    Serial.println(Ethernet.localIP());
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }
  
  // give the Ethernet shield a second to initialize, and check leds
  turnstile.write(0);
  delay(1500);
  turnstile.write(180);
  delay(1500);
  turnstile.write(0);
  
  allow();
  disallow();
  delay(1000);
  allow();
  disallow();
}

/**
 * Main loop.
 */
void loop() {  
  //send ping request every 1 min
  if (millis() - lastPingTime > 60000) {
    sendPing();
    // note the time that the ping was made
    lastPingTime = millis();
  }
  
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
      return;

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
      return;

  // Show some details of the PICC (that is: the tag/card)
  Serial.print(F("Card UID:"));
  dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);

  if (false == compareUids(mfrc522.uid.uidByte, mfrc522.uid.size)) {
    disallow();
    return;
  }
  Serial.println();
  MFRC522::PICC_Type piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);

  // Check for compatibility
  if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
      &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
      &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
      Serial.println(F("works only with MIFARE Classic cards."));
      disallow();
      return;
  }
    
  // that is: sector #1, covering block #4 up to and including block #7
  byte sector         = 1;
  byte trailerBlock   = 7;
  byte blockAddr      = 4;
  MFRC522::StatusCode status;
  byte buffer[18];
  byte size = sizeof(buffer);

  // Authenticate using key B
  Serial.println(F("Authenticating using key B..."));
  status = (MFRC522::StatusCode) mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
      Serial.print(F("PCD_Authenticate() failed: "));
      disallow();
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
  }

  // Read data
  status = (MFRC522::StatusCode) mfrc522.MIFARE_Read(4, buffer, &size);
  if (status != MFRC522::STATUS_OK) {
      Serial.print(F("MIFARE_Read() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
      disallow();
      return;
  }
    
  if (hasAccess(mfrc522.uid.uidByte, mfrc522.uid.size, buffer, 16)) {
    allow();
  } else {
    disallow();
  }

  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
}

/**
 * Helper routine to dump a byte array as hex values to Serial.
 */
void dump_byte_array(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

boolean hasAccess(byte *uidBuffer, byte uidBufferSize, byte *buffer, byte bufferSize) {
  dump_byte_array(buffer, 16); Serial.println();
  if (false == successPing) {
    Serial.println("No internet connection. Can not send access request check. Check only card uids.");
    return true;
  }
  hasAccessRequest(uidBuffer, uidBufferSize, buffer, bufferSize);
  
  delay(100); //wait till response
  
  //check response
  while(client.available()){
    String responseLine = client.readStringUntil('\r');
    Serial.println(responseLine);
    client.flush();
    
    if (responseLine.indexOf("Uid-has-access: 1") != -1) {
      return true;
    }
  }
  
  return false;
}

boolean compareUids(byte *buffer, byte bufferSize) {
  String uid = formatBufferToString(buffer, bufferSize); 

  for (byte i = 0; i < 10; i++) {
    Serial.println();
    if (uids[i] == uid) {
      return true;
    }
  }
  Serial.println();
  Serial.println("UID tag :'" + uid + "'");
  return false;
}

String formatBufferToString(byte *buffer, byte bufferSize) {
  String uid = ""; 
    
  for (byte i = 0; i < bufferSize; i++) {
     uid.concat(String(buffer[i], HEX));
  }

  return uid;
}


// this method makes a HTTP connection to the server:
void hasAccessRequest(byte *uidBuffer, byte uidBufferSize, byte *buffer, byte bufferSize) {
  // close any connection before send a new request.
  client.stop();

  // if there's a successful connection:
  if (client.connect(server, 80)) {
    Serial.println("connecting...");
    String getRequestString =  "/hasAccess/"
      + readerUid + "/"
      + formatBufferToString(uidBuffer, uidBufferSize) + "/"
      + formatBufferToString(buffer, bufferSize);
    Serial.println(getRequestString);
    
    // send the HTTP GET request:
    client.println("GET " + getRequestString + " HTTP/1.1");
    client.println("Host: www.grep.lv");
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: close");
    client.println();
    Serial.println("sent...");
  } else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
  }
}

void sendPing() {
  // close any connection before send a new request.
  client.stop();
  Serial.println("try send ping");

  // if there's a successful connection:
  if (client.connect(server, 80)) {
    Serial.println("connecting...");
    String getRequestString =  "/ping/" + controllerUid;
    Serial.println(getRequestString);
    
    // send the HTTP GET request:
    client.println("GET " + getRequestString + " HTTP/1.1");
    client.println("Host: www.grep.lv");
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: close");
    client.println();
    Serial.println("sent...");
    
    //check response
    while(client.available()){
      String responseLine = client.readStringUntil('\r');
      Serial.println(responseLine);
      client.flush();
      
      if (responseLine.indexOf("200 OK") != -1) {
        successPing = true;
      }
    }
  } else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
    successPing = false;
  }
}

void allow() {
    Serial.print("Allow");
    digitalWrite(GREEN_PIN, HIGH);
    turnstile.write(0); //0 degrees - open turnstile
    delay(4000);
    digitalWrite(GREEN_PIN, LOW);
    turnstile.write(90); //90 degrees - close turnstile
}

void disallow() {
    Serial.print("Disallow");
    digitalWrite(RED_PIN, HIGH);
    delay(500);
    digitalWrite(RED_PIN, LOW);
}