<?php

namespace App\Controller;

use App\Component\Http\JsonApiResponse;
use App\Entity\CardActionLog;
use App\Entity\ControllerDevice;
use App\Entity\RfidCard;
use App\Entity\RfidReader;
use App\Repository\CardActionLogRepository;
use App\Repository\ControllerDeviceRepository;
use App\Repository\RfidCardRepository;
use App\Repository\RfidReaderRepository;
use App\Service\Logger;
use App\Service\PolicyDecision;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;

class ReadersController extends AbstractController
{
    /**
     * @Route("hasAccess/{readerUuid}/{cardUuid}/{secretData}", methods={"GET"}, name="has.access")
     * @ParamConverter("rfidReader", options={"mapping": {"readerUuid": "readerUuid"}})
     * @ParamConverter("rfidCard", options={"mapping": {"cardUuid": "cardUuid"}})
     * @param RfidReader              $rfidReader
     * @param RfidCard                $rfidCard
     * @param string                  $secretData
     * @param PolicyDecision          $decision
     * @param CardActionLogRepository $cardActionLogRepository
     * @param Logger                  $logger
     * @param Request                 $request
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function hasAccessAction(
        RfidReader              $rfidReader,
        RfidCard                $rfidCard,
        string                  $secretData,
        PolicyDecision          $decision,
        CardActionLogRepository $cardActionLogRepository,
        Logger                  $logger,
        Request                 $request
    ): Response
    {
        $logger->setReader($rfidReader)->info('start logging request', [
            'rfidReader' => $rfidReader->logContext(),
            'rfidCard'   => $rfidCard->logContext(),
//            'request'    => $request
        ]);
        $hasAccess = $decision->hasAccess($rfidReader, $rfidCard, $secretData, $request->request->all());

        $response = JsonApiResponse::content(['Uid-has-access' => $hasAccess]);
        $response->headers->set('Uid-has-access', $hasAccess);
        $cardActionLog = CardActionLog::create($rfidCard, $rfidReader, $hasAccess);
        $cardActionLogRepository->create($cardActionLog);
        $logger->info('made  decision', $cardActionLog->logContext());

        return $response;
    }

    /**
     * @Route("ping/{uuid}", methods={"GET"}, name="ping")
     * @param ControllerDevice           $controllerDevice
     * @param ControllerDeviceRepository $repository
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function pingAction(ControllerDevice $controllerDevice, ControllerDeviceRepository $repository): Response
    {
        $repository->update($controllerDevice->ping());
        return JsonApiResponse::content(['pong']);
    }

    /**
     * @Route("cardUuids/{readerUuid}", methods={"GET"}, name="get.rfid_card_uuids")
     * @param RfidReader         $rfidReader
     * @param RfidCardRepository $repository
     * @param Request            $request
     * @return Response
     */
    public function getRfidCardUuidsAction(RfidReader $rfidReader, RfidCardRepository $repository, Request $request): Response
    {
        $rfidCardUuids = [];

        /** @var RfidCard $rfidCard */
        foreach ($repository->findAll() as $rfidCard) {
            $rfidCardUuids[] = $rfidCard->getCardUuid();
        }

        return JsonApiResponse::content($rfidCardUuids);
    }
}