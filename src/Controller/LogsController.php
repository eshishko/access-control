<?php

namespace App\Controller;

use App\Entity\RfidReader;
use App\Service\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

final class LogsController extends AbstractController
{
    /**
     * @Route("logs/{id}/{fileName}", methods={"GET"}, name="get.log")
     * @return Response
     */
    public function hasAccessAction(RfidReader $reader, string $fileName, Logger $logger): Response
    {
        $fileSystem = new Filesystem();
        $logFile    = sprintf('%s/reader_%s/%s.txt', $logger->logDir, $reader->getId(), $fileName);
        if (false === $fileSystem->exists($logFile)) {
            throw new NotFoundResourceException('Log not found!');
        }

        return $this->render('logs/logs.html.twig', [
            'logs' => $this->parseLogFile($logFile)
        ]);
    }

    /**
     * @param string $logFile
     * @return array
     */
    private function parseLogFile(string $logFile): array
    {
        $logs         = [];
        $segmentIndex = 0;

        foreach(\file($logFile) as $line) {
            if ($line[0] === '[') {
                $timestampEnd = \strpos($line, ']');
                $levelEnd     = \strpos($line, ']', $timestampEnd+1) + 1;

                $timestamp = trim(\substr($line,0, $levelEnd+1));
                $level     = trim(\substr($timestamp, strrpos($timestamp, '['), $levelEnd+1));
                $level     = \str_replace(['[', ']'], ['', ''], $level);
                $message   = trim(\substr($line, $levelEnd+1));

                if (false === \in_array($level, ['info', 'debug', 'notice', 'warning'], true)) {
                    $level = 'notice';
                }

                $logs[$segmentIndex] = [
                    'timestamp' => $timestamp,
                    'level'     => $level,
                    'message'   => $message,
                    'content'   => ''
                ];
                ++$segmentIndex;
            } else {
                $logs[$segmentIndex - 1]['content'] .= $line;
            }
        }

        return $logs;
    }
}