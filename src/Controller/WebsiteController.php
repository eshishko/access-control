<?php

namespace App\Controller;

use App\Repository\ControllerDeviceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class WebsiteController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="website")
     * @param ControllerDeviceRepository $repository
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('index.html.twig', ['success' => 1]);
    }
}