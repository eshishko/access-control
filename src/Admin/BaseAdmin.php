<?php

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;

abstract class BaseAdmin extends AbstractAdmin
{
    /**
     * @return bool
     */
    protected function isCreate(): bool
    {
        return !$this->isEdit();
    }

    /**
     * @return bool
     */
    protected function isEdit(): bool
    {
        return (bool)$this->id($this->getSubject());
    }
}