<?php

namespace App\Admin;

use App\Entity\RfidReader;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CompanyUsersAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('id');
        $filter->add('status');
        $filter->add('firstName');
        $filter->add('lastName');
        $filter->add('gender');
        $filter->add('rfidCard.user', null, ['label' => 'Card Owner']);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('firstName');
        $listMapper->add('lastName');
        $listMapper->add('status');
        $listMapper->add('rfidCard');
        $listMapper->add('updatedAt', null, [
            'label' => 'Updated at',
        ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('status', null, ['disabled' => $this->isCreate()])
            ->add('firstName')
            ->add('gender', ChoiceType::class, ['choices' => ['Male' => 'm', 'Female' => 'f']])
            ->add('middleName')
            ->add('lastName')
            ->add('phone')
            ->add('rfidCard', null, ['label' => 'RFID Card', 'required' => false]);
        ;
    }
}