<?php

namespace App\Admin;

use App\Entity\RfidReader;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\Type\ModelType;

class RfidReaderAdmin extends BaseAdmin
{
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('readerUuid');
        $listMapper->add('status');
        $listMapper->add('Ping Status', null, ['template' => 'admin/RfidReader/List/ping.html.twig']);
        $listMapper->add('description', null, ['collapsed' => true]);
        $listMapper->add('updatedAt', null, ['label' => 'Updated at']);
        $listMapper->add('_action', null, [
            'actions' => [
                'show' => [],
            ]
        ]);
    }

    /**
     * @param ShowMapper $showMapper
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper->add('id');
        $showMapper->add('readerUuid');
        $showMapper->add('status');
        $showMapper->add('Ping Status', null, ['template' => 'admin/RfidReader/Show/ping.html.twig']);
        $showMapper->add('actionLogs', null, ['template' => 'admin/RfidReader/Show/actionLogs.html.twig']);
        $showMapper->add('fluentLogs', null, ['template' => 'admin/RfidReader/Show/fluentLogs.html.twig']);
        $showMapper->add('description', null, ['collapsed' => true]);
        $showMapper->add('updatedAt', null, ['label' => 'Updated at']);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('readerUuid')
            ->add('status', null, ['disabled' => $this->isCreate()])
            ->add('description');
    }

    /**
     * @param $object
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof RfidReader
            ? sprintf('RFID Reader: %s', $object->getReaderUuid())
            : 'RFID Reader';
    }
}