<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;

class CardActionLogAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $filter
     */
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter->add('id');
        $filter->add('rfidCard');
        $filter->add('rfidCard.user', null, ['label' => 'Card Owner']);
        $filter->add('rfidReader');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('actionResult', null, ['template' => 'admin/Logs/List/actionResult.html.twig']);
        $listMapper->add('rfidCard.user', null, ['label' => 'Card Owner']);
        $listMapper->add('rfidCard.cardUuid', null, ['label' => 'RFID Card']);
        $listMapper->add('rfidReader.readerUuid', null, ['label' => 'RFID Reader']);
        $listMapper->add('createdAt');
    }

    /**
     * @param ShowMapper $showMapper
     * @throws \RuntimeException
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id', null, ['label' => 'ID'])
            ->add('actionResult')
            ->add('rfidCard.user', null, ['label' => 'Card Owner'])
            ->add('rfidCard.cardUuid', null, ['label' => 'RFID Card'])
            ->add('rfidReader.readerUuid', null, ['label' => 'RFID Reader'])
            ->add('createdAt');
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['show', 'list']);
    }
}