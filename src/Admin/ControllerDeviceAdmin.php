<?php

namespace App\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;

class ControllerDeviceAdmin extends BaseAdmin
{
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('uuid');
        $listMapper->add('status');
        $listMapper->add('location');
        $listMapper->add('description', null, ['collapsed' => true]);
        $listMapper->add('updatedAt', null, [
            'label' => 'Updated at',
        ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('uuid')
            ->add('status', null, ['disabled' => $this->isCreate()])
            ->add('location')
            ->add('description');

        $formMapper
            ->add('readers', ModelType::class, [
                'label'    => 'RFID Reader',
                'multiple' => true,
            ]);
    }

    /**
     * @inheritDoc
     */
    public function prePersist($object)
    {
        foreach ($object->getReaders() as $reader) {
            $object->addReader($reader);
        }
    }

    /**
     * @inheritDoc
     */
    public function preUpdate($object)
    {
        foreach ($object->getReaders() as $reader) {
            $object->addReader($reader);
        }
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(['create', 'list', 'edit']);
    }
}