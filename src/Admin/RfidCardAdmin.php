<?php

namespace App\Admin;

use App\Entity\RfidCard;
use App\Entity\RfidReader;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\Type\ModelType;

class RfidCardAdmin extends BaseAdmin
{
    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('cardUuid');
        $listMapper->add('user');
        $listMapper->add('status', null, ['editable' => true]);
        $listMapper->add('logs', null, ['template' => 'admin/RfidCard/List/logs.html.twig']);
        $listMapper->add('updatedAt', null, [
            'label' => 'Updated at',
        ]);
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('cardUuid')
            ->add('status', null, ['disabled' => $this->isCreate()])
            ->add('secretData')
            ->add('user', null, ['label' => 'Company User', 'required' => false]);
    }

    /**
     * @param $object
     * @return string
     */
    public function toString($object): string
    {
        return $object instanceof RfidCard
            ? sprintf('RFID Card: %s', $object->getCardUuid())
            : 'RFID Card';
    }
}