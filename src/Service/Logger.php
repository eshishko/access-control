<?php

namespace App\Service;

use App\Entity\FluentLog;
use App\Entity\RfidReader;
use App\Repository\RfidReaderRepository;
use Katzgrau\KLogger\Logger as KLogger;

final class Logger
{
    /** @var string $logDir */
    public $logDir;
    /** @var KLogger|null $logger */
    private $logger;
    /** @var RfidReader|null $reader */
    private $reader;
    /** @var RfidReaderRepository $rfidReaderRepository */
    private $rfidReaderRepository;

    public function __construct(string $logDir, RfidReaderRepository $rfidReaderRepository)
    {
        $this->rfidReaderRepository = $rfidReaderRepository;
        $this->logDir               = $logDir;
    }

    /**
     * @param RfidReader $reader
     * @return Logger
     */
    public function setReader(RfidReader $reader): Logger
    {
        $this->reader = $reader;
        return $this;
    }

    /**
     * @param string $level
     * @param string $message
     * @param array  $context
     */
    public function log(string $level, string $message, array $context = []): void
    {
        $this->logger = new KLogger($this->logDir);
        if (null !== $this->reader) {
            $this->logger = new KLogger(sprintf('%s/reader_%s', $this->logDir, $this->reader->getId()));
            $fileName = $this->logger->getLogFilePath();
            $this->reader->addFluentLog($fileName);
            $this->rfidReaderRepository->update($this->reader);
        }

        $this->logger->$level($message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function info(string $message, array $context = []): void
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function debug(string $message, array $context = []): void
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function notice(string $message, array $context = []): void
    {
        $this->log(__FUNCTION__, $message, $context);
    }

    /**
     * @param string $message
     * @param array  $context
     * @return void
     */
    public function warning(string $message, array $context = []): void
    {
        $this->log(__FUNCTION__, $message, $context);
    }
}