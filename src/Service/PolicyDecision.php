<?php

namespace App\Service;

use App\Entity\RfidCard;
use App\Entity\RfidReader;

final class PolicyDecision
{
    /**
     * @param RfidReader $reader
     * @param RfidCard   $rfidCard
     * @param string     $secretData
     * @param array      $requestData
     * @return bool
     */
    public function hasAccess(RfidReader $reader, RfidCard $rfidCard, string $secretData, array $requestData): bool
    {
        return \strtolower($rfidCard->getStatus()) === 'open'
            && \strtolower($rfidCard->getSecretData()) === \strtolower($secretData) ;
    }
}