<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as Doctrine;
use DateTime;

/**
 * @Doctrine\Entity(repositoryClass="App\Repository\CardActionLogRepository")
 * @Doctrine\Table(name="card_action_log")
 * @Doctrine\HasLifecycleCallbacks()
 */
class CardActionLog
{
    /**
     * @var integer $id
     *
     * @Doctrine\Id()
     * @Doctrine\Column(type="smallint", options={"unsigned": true})
     * @Doctrine\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var RfidCard $rfidCard
     * @Doctrine\ManyToOne(targetEntity="App\Entity\RfidCard", inversedBy="actionLogs")
     * @Doctrine\JoinColumn(name="rfid_card_id", referencedColumnName="id", nullable=false)
     */
    private $rfidCard;
    /**
     * @var RfidReader $rfidReader
     * @Doctrine\ManyToOne(targetEntity="App\Entity\RfidReader", inversedBy="actionLogs")
     * @Doctrine\JoinColumn(name="rfid_reader_id", referencedColumnName="id", nullable=false)
     */
    private $rfidReader;
    /**
     * @var bool $actionResult
     * @Doctrine\Column(type="boolean", nullable=false)
     */
    private $actionResult;
    /**
     * @var DateTime $createdAt
     * @Doctrine\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    public function __construct(RfidCard $rfidCard, RfidReader $rfidReader, bool $actionResult)
    {
        $this->rfidCard     = $rfidCard;
        $this->rfidReader   = $rfidReader;
        $this->actionResult = $actionResult;
        $this->createdAt    = new DateTime();
    }

    /**
     * @param RfidCard   $rfidCard
     * @param RfidReader $rfidReader
     * @param bool       $actionResult
     * @return CardActionLog
     */
    public static function create(RfidCard $rfidCard, RfidReader $rfidReader, bool $actionResult): CardActionLog
    {
        return new self($rfidCard, $rfidReader, $actionResult);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return RfidCard
     */
    public function getRfidCard(): RfidCard
    {
        return $this->rfidCard;
    }

    /**
     * @return RfidReader
     */
    public function getRfidReader(): RfidReader
    {
        return $this->rfidReader;
    }

    /**
     * @return bool
     */
    public function getActionResult(): ?bool
    {
        return $this->actionResult;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return array
     */
    public function logContext(): array
    {
        return [
            'id'         => $this->id,
            'decision'   => $this->actionResult,
            'rfidReader' => $this->rfidReader->getReaderUuid(),
            'rfidCard'   => $this->rfidCard->getCardUuid(),
            'user'       => (string)$this->rfidCard->getUser(),
        ];
    }
}