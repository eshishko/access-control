<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as Doctrine;
use DateTime;

/**
 * @Doctrine\Entity(repositoryClass="App\Repository\CompanyUsersRepository")
 * @Doctrine\HasLifecycleCallbacks()
 */
class CompanyUsers
{
    /**
     * @Doctrine\Id()
     * @Doctrine\GeneratedValue()
     * @Doctrine\Column(type="integer")
     */
    private $id;
    /**
     * @var RfidCard|null $user
     * @Doctrine\OneToOne(targetEntity="App\Entity\RfidCard")
     * @Doctrine\JoinColumn(name="rfid_reader_id", referencedColumnName="id", nullable=true)
     */
    private $rfidCard;
    /**
     * @var string $status
     * @Doctrine\Column(type="string", name="status", nullable=false)
     */
    private $status;
    /**
     * @var string $firstName
     * @Doctrine\Column(type="string", name="first_name", nullable=false)
     */
    private $firstName;
    /**
     * @var string|null $middleName
     * @Doctrine\Column(type="string", name="middle_name", nullable=true)
     */
    private $middleName;
    /**
     * @var string $lastName
     * @Doctrine\Column(type="string", name="last_name", nullable=false)
     */
    private $lastName;
    /**
     * @var string $gender
     * @Doctrine\Column(type="string", name="gender", nullable=false)
     */
    private $gender;
    /**
     * @var string|null $phone
     * @Doctrine\Column(type="string", name="phone", nullable=true)
     */
    private $phone;
    /**
     * @var DateTime $createdAt
     * @Doctrine\Column(type="datetime", name="created_at")
     */
    private $createdAt;
    /**
     * @var DateTime $updatedAt
     * @Doctrine\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->status    = 'active';
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param RfidCard|null $rfidCard
     * @return CompanyUsers
     */
    public function setRfidCard(?RfidCard $rfidCard): CompanyUsers
    {
        if (null !== $rfidCard) {
            $rfidCard->setUser($this);
        }
        $this->rfidCard = $rfidCard;

        return $this;
    }

    /**
     * @return RfidCard|null
     */
    public function getRfidCard(): ?RfidCard
    {
        return $this->rfidCard;
    }

    /**
     * @param string $status
     * @return CompanyUsers
     */
    public function setStatus(string $status): CompanyUsers
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return CompanyUsers
     */
    public function setFirstName(string $firstName): CompanyUsers
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return CompanyUsers
     */
    public function setLastName(string $lastName): CompanyUsers
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @param string|null $middleName
     * @return CompanyUsers
     */
    public function setMiddleName(?string $middleName): CompanyUsers
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return CompanyUsers
     */
    public function setGender(string $gender): CompanyUsers
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return CompanyUsers
     */
    public function setPhone(?string $phone): CompanyUsers
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @Doctrine\PreUpdate()
     * @return void
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return \sprintf('%s %s', $this->getFirstName(), $this->getLastName());
    }
}