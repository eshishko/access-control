<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as Doctrine;
use Symfony\Component\Security\Core\User\UserInterface;
use DateTime;

/**
 * @Doctrine\Entity(repositoryClass="App\Repository\UserRepository")
 * @Doctrine\HasLifecycleCallbacks()
 */
class User implements UserInterface
{
    /**
     * @Doctrine\Id()
     * @Doctrine\GeneratedValue()
     * @Doctrine\Column(type="integer")
     */
    private $id;
    /**
     * @var string The hashed password
     * @Doctrine\Column(type="string")
     */
    private $password;
    /**
     * @Doctrine\Column(type="json")
     */
    private $roles = [];
    /**
     * @var string $login
     * @Doctrine\Column(type="string", name="login", nullable=false, length=64, unique=true)
     */
    private $login;
    /**
     * @var string $name
     * @Doctrine\Column(type="string", name="name", nullable=false)
     */
    private $name;
    /**
     * @var DateTime $createdAt
     * @Doctrine\Column(type="datetime", name="created_at")
     */
    private $createdAt;
    /**
     * @var DateTime $updatedAt
     * @Doctrine\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->getLogin();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return User
     */
    public function setLogin(string $login): User
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @Doctrine\PreUpdate()
     * @return void
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }
}
