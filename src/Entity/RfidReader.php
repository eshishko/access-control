<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Doctrine;
use Doctrine\ORM\Mapping\Index;
use DateTime;

/**
 * @Doctrine\Entity(repositoryClass="App\Repository\RfidReaderRepository")
 * @Doctrine\Table(name="rfid_reader", indexes={
 *         @Index(name="status_idx", columns={"status"})
 *     }
 * )
 * @Doctrine\HasLifecycleCallbacks()
 */
class RfidReader
{
    /**
     * @var integer $id
     *
     * @Doctrine\Id()
     * @Doctrine\Column(type="smallint", options={"unsigned": true})
     * @Doctrine\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string $uuid
     * @Doctrine\Column(type="string", length=32, nullable=false, unique=true)
     */
    private $readerUuid;
    /**
     * @var string $status
     * @Doctrine\Column(type="string", length=32, nullable=false)
     */
    private $status;
    /**
     * @var string $description
     * @Doctrine\Column(type="string", nullable=false)
     */
    private $description;
    /**
     * @var ControllerDevice $controllerDevice
     * @Doctrine\ManyToOne(targetEntity="App\Entity\ControllerDevice", inversedBy="readers")
     * @Doctrine\JoinColumn(nullable=true, name="controller_device_id")
     */
    private $controllerDevice;
    /**
     * @var ArrayCollection|CardActionLog[]|null $actionLogs
     * @Doctrine\OneToMany(targetEntity="App\Entity\CardActionLog", mappedBy="rfidReader")
     * @Doctrine\JoinColumn(nullable=true)
     */
    private $actionLogs;
    /**
     * @var ArrayCollection|FluentLog[]|null $fluentLogs
     * @Doctrine\OneToMany(targetEntity="App\Entity\FluentLog", mappedBy="rfidReader", cascade={"all"})
     * @Doctrine\JoinColumn(nullable=true)
     */
    private $fluentLogs;
    /**
     * @var DateTime $createdAt
     * @Doctrine\Column(type="datetime", name="created_at")
     */
    private $createdAt;
    /**
     * @var DateTime|null $updatedAt
     * @Doctrine\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->status     = 'open';
        $this->actionLogs = new ArrayCollection();
        $this->fluentLogs = new ArrayCollection();
        $this->createdAt  = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getReaderUuid()
    {
        return $this->readerUuid;
    }

    /**
     * @param string $readerUuid
     * @return RfidReader
     */
    public function setReaderUuid(string $readerUuid): RfidReader
    {
        $this->readerUuid = $readerUuid;

        return $this;
    }

    /**
     * @return CardActionLog[]|ArrayCollection|null
     */
    public function getActionLogs()
    {
        return $this->actionLogs;
    }

    /**
     * @param string $fluentLogPath
     * @return RfidReader
     */
    public function addFluentLog(string $fluentLogPath): RfidReader
    {
        $fluentLog = $this->fluentLogs->filter(
            function (FluentLog $fluentLog) use ($fluentLogPath) {
                return $fluentLogPath === $fluentLog->getPath();
            }
        )->first();

        if (false === $fluentLog) {
            $this->fluentLogs->add(FluentLog::create($this, $fluentLogPath));
        }

        return $this;
    }

    /**
     * @return FluentLog[]|ArrayCollection|null
     */
    public function getFluentLogs()
    {
        return $this->fluentLogs;
    }

    /**
     * @param string $status
     * @return RfidReader
     */
    public function setStatus(string $status): RfidReader
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $description
     * @return RfidReader
     */
    public function setDescription(string $description): RfidReader
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param ControllerDevice $controllerDevice
     * @return RfidReader
     */
    public function setControllerDevice(ControllerDevice $controllerDevice): RfidReader
    {
        $this->controllerDevice = $controllerDevice;

        return $this;
    }

    /**
     * @return ControllerDevice|null
     */
    public function getControllerDevice()
    {
        return $this->controllerDevice;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @Doctrine\PreUpdate()
     * @return void
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getReaderUuid();
    }

    /**
     * @return array
     */
    public function logContext(): array
    {
        return [
            'id'             => $this->id,
            'uuid'           => $this->getReaderUuid(),
            'controllerUuid' => $this->controllerDevice->getUuid(),
            'status'         => $this->status,
        ];
    }
}