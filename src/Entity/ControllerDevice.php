<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Doctrine;
use Doctrine\ORM\Mapping\Index;
use DateTime;

//TODO:: add service user credentials
/**
 * @Doctrine\Entity(repositoryClass="App\Repository\ControllerDeviceRepository")
 * @Doctrine\Table(name="controller_device", indexes={
 *         @Index(name="status_idx", columns={"status"})
 *     }
 * )
 * @Doctrine\HasLifecycleCallbacks()
 */
class ControllerDevice
{
    /**
     * @var integer $id
     *
     * @Doctrine\Id()
     * @Doctrine\Column(type="smallint", options={"unsigned": true})
     * @Doctrine\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string $uuid
     * @Doctrine\Column(type="string", length=32, nullable=false, unique=true)
     */
    private $uuid;
    /**
     * @var string $status
     * @Doctrine\Column(type="string", length=32, nullable=false)
     */
    private $status;
    /**
     * @var string $location
     * @Doctrine\Column(type="string", nullable=false)
     */
    private $location;
    /**
     * @var string $description
     * @Doctrine\Column(type="string", nullable=false)
     */
    private $description;
    /**
     * @var ArrayCollection|RfidReader[] $readers
     * @Doctrine\OneToMany(targetEntity="App\Entity\RfidReader", cascade={"persist", "merge", "remove"}, mappedBy="controllerDevice")
     * @Doctrine\JoinColumn(nullable=false)
     */
    private $readers;
    /**
     * @var DateTime $lastPingAt
     * @Doctrine\Column(type="datetime", name="last_ping_at", nullable=true)
     */
    private $lastPingAt;
    /**
     * @var DateTime $createdAt
     * @Doctrine\Column(type="datetime", name="created_at")
     */
    private $createdAt;
    /**
     * @var DateTime|null $updatedAt
     * @Doctrine\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->status    = 'open';
        $this->readers   = new ArrayCollection();
        $this->createdAt = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $uuid
     * @return ControllerDevice
     */
    public function setUuid(string $uuid): ControllerDevice
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $status
     * @return ControllerDevice
     */
    public function setStatus(string $status): ControllerDevice
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $location
     * @return ControllerDevice
     */
    public function setLocation(string $location): ControllerDevice
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param string $description
     * @return ControllerDevice
     */
    public function setDescription(string $description): ControllerDevice
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param RfidReader[]|ArrayCollection $readers
     * @return ControllerDevice
     */
    public function setReaders($readers): ControllerDevice
    {
        if (count($readers) > 0) {
            foreach ($readers as $reader) {
                $this->addReader($reader);
            }
        }

        return $this;
    }

    /**
     * @param RfidReader $reader
     * @return ControllerDevice
     */
    public function addReader(RfidReader $reader): ControllerDevice
    {
        $reader->setControllerDevice($this);
        if (false === $this->readers->contains($reader)) {
            $this->readers->add($reader);
        }

        return $this;
    }

    /**
     * @param RfidReader $reader
     * @return ControllerDevice
     */
    public function removeReader(RfidReader $reader): ControllerDevice
    {
        $this->readers->removeElement($reader);
        return $this;
    }

    /**
     * @return RfidReader[]|ArrayCollection
     */
    public function getReaders()
    {
        return $this->readers;
    }

    /**
     * @return DateTime|null
     */
    public function getLastPingAt()
    {
        return $this->lastPingAt;
    }

    /**
     * @return ControllerDevice
     * @throws \Exception
     */
    public function ping(): ControllerDevice
    {
        $this->lastPingAt = new DateTime();
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @Doctrine\PreUpdate()
     * @return void
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}