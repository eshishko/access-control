<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as Doctrine;
use DateTime;

/**
 * @Doctrine\Entity(repositoryClass="App\Repository\RfidCardRepository")
 * @Doctrine\HasLifecycleCallbacks()
 */
class RfidCard
{
    /**
     * @Doctrine\Id()
     * @Doctrine\GeneratedValue()
     * @Doctrine\Column(type="integer")
     */
    private $id;
    /**
     * @Doctrine\Column(type="string", length=64, unique=true)
     */
    private $cardUuid;
    /**
     * @var CompanyUsers|null $user
     * @Doctrine\OneToOne(targetEntity="App\Entity\CompanyUsers")
     * @Doctrine\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;
    /**
     * @var ArrayCollection|CardActionLog[]|null $actionLogs
     * @Doctrine\OneToMany(targetEntity="App\Entity\CardActionLog", mappedBy="rfidCard")
     * @Doctrine\JoinColumn(nullable=true)
     */
    private $actionLogs;
    /**
     * @var string $secretData
     * @Doctrine\Column(type="string", name="secret_data", nullable=false)
     */
    private $secretData;
    /**
     * @var string $status
     * @Doctrine\Column(type="string", name="status", nullable=false)
     */
    private $status;
    /**
     * @var DateTime $createdAt
     * @Doctrine\Column(type="datetime", name="created_at")
     */
    private $createdAt;
    /**
     * @var DateTime $updatedAt
     * @Doctrine\Column(type="datetime", name="updated_at", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->status     = 'open';
        $this->actionLogs = new ArrayCollection();
        $this->createdAt  = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCardUuid()
    {
        return $this->cardUuid;
    }

    /**
     * @param mixed $cardUuid
     * @return RfidCard
     */
    public function setCardUuid($cardUuid): RfidCard
    {
        $this->cardUuid = $cardUuid;

        return $this;
    }

    /**
     * @return CardActionLog[]|ArrayCollection|null
     */
    public function getActionLogs()
    {
        return $this->actionLogs;
    }

    /**
     * @return string|null
     */
    public function getSecretData()
    {
        return $this->secretData;
    }

    /**
     * @param string $secretData
     * @return RfidCard
     */
    public function setSecretData(string $secretData): RfidCard
    {
        $this->secretData = $secretData;

        return $this;
    }

    /**
     * @param CompanyUsers|null $user
     * @return RfidCard
     */
    public function setUser(?CompanyUsers $user): RfidCard
    {
        if (null !== $user && null !== $user->getRfidCard() && $user->getRfidCard()->getCardUuid() !== $this->getCardUuid()) {
            $user->setRfidCard($this);
        }
        $this->user = $user;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?CompanyUsers
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return RfidCard
     */
    public function setStatus(string $status): RfidCard
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @Doctrine\PreUpdate()
     * @return void
     */
    public function setUpdatedAt(): void
    {
        $this->updatedAt = new DateTime();
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getCardUuid();
    }

    /**
     * @return array
     */
    public function logContext(): array
    {
        return [
            'id'       => $this->id,
            'uuid'     => $this->getCardUuid(),
            'userName' => (string)$this->user,
            'status'   => $this->status,
        ];
    }
}