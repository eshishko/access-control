<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as Doctrine;
use DateTime;

/**
 * @Doctrine\Entity(repositoryClass="App\Repository\FluentLogRepository")
 * @Doctrine\HasLifecycleCallbacks()
 */
class FluentLog
{
    /**
     * @var integer $id
     *
     * @Doctrine\Id()
     * @Doctrine\Column(type="smallint", options={"unsigned": true})
     * @Doctrine\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string $path
     * @Doctrine\Column(type="string", length=255, nullable=false)
     */
    private $path;
    /**
     * @var RfidReader $rfidReader
     * @Doctrine\ManyToOne(targetEntity="App\Entity\RfidReader", inversedBy="fluentLogs")
     * @Doctrine\JoinColumn(name="rfid_reader_id", referencedColumnName="id", nullable=false)
     */
    private $rfidReader;
    /**
     * @var DateTime $createdAt
     * @Doctrine\Column(type="datetime", name="created_at")
     */
    private $createdAt;


    public function __construct(RfidReader $rfidReader, string $path)
    {
        $this->path       = $path;
        $this->rfidReader = $rfidReader;
        $this->createdAt  = new DateTime();
    }

    /**
     * @param RfidReader $rfidReader
     * @param string     $path
     * @return FluentLog
     */
    public static function create(RfidReader $rfidReader, string $path): FluentLog
    {
        return new self($rfidReader, $path);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return RfidReader
     */
    public function getRfidReader(): RfidReader
    {
        return $this->rfidReader;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }
}