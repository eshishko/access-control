<?php

namespace App\Twig;

use App\Entity\ControllerDevice;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use DateTime;
use Throwable;

class AdminTwigExtension extends AbstractExtension
{
    /** @var string $rootDirectoryPath */
    private $rootDirectoryPath;

    public function __construct(string $rootDirectoryPath)
    {
        $this->rootDirectoryPath = $rootDirectoryPath;
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('pingStatus', [$this, 'pingStatus']),
            new TwigFunction('getLogFile', [$this, 'getLogFile']),
            new TwigFunction('productionTag', [$this, 'productionTag']),
        ];
    }

    /**
     * @param ControllerDevice $controllerDevice
     * @return bool
     */
    public function pingStatus(ControllerDevice $controllerDevice): bool
    {
        if (!$controllerDevice->getLastPingAt()) {
            return false;
        }

        $lastPingTimestamp = $controllerDevice->getLastPingAt()->getTimestamp();
        return (\time() - $lastPingTimestamp) <= 6*60;
    }

    /**
     * @param string $filePath
     * @return string
     */
    public function getLogFile(string $filePath): string
    {
        return \pathinfo($filePath)['filename'] ?? '';
    }

    /**
     * @return string
     */
    public function productionTag(): string
    {
        $file = sprintf('%s/CURRENT_PRODUCTION_TAG', $this->rootDirectoryPath);
        if (true === is_readable($file)) {
            return trim((string)file_get_contents($file));
        }

        return '';


    }
}
