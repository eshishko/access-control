<?php

namespace App\Component\Http;

use Symfony\Component\HttpFoundation\JsonResponse;

class JsonApiResponse extends JsonResponse
{
    /**
     * @param mixed $data
     * @param int   $status
     * @throws \InvalidArgumentException
     */
    public function __construct($data, $status = self::HTTP_OK)
    {
        parent::__construct($data, $status, ['Access-Control-Allow-Origin' => '*']);
    }

    /**
     * @param mixed  $data
     * @param int    $status
     * @return JsonApiResponse
     */
    public static function content($data = '', int $status = self::HTTP_OK): JsonApiResponse
    {
        return new self([
            'success' => true,
            'result'  => $data
        ], $status);
    }

    /**
     * @param mixed $data
     * @param int   $status
     * @return JsonApiResponse
     */
    public static function validation($data, int $status = self::HTTP_BAD_REQUEST): JsonApiResponse
    {
        return new self([
            'success' => false,
            'result'  => $data
        ], $status);
    }

    /**
     * @param mixed  $data
     * @param int    $status
     * @return JsonApiResponse
     */
    public static function exception($data = '', int $status = self::HTTP_INTERNAL_SERVER_ERROR): JsonApiResponse
    {
        return new self([
            'success' => false,
            'result'  => $data
        ], $status);
    }
}
