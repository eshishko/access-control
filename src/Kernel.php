<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\RouteCollectionBuilder;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    const CONFIG_EXTS = '.{php,xml,yaml,yml}';

    public function getCacheDir()
    {
        $baseDir = $this->getProjectDir();
        if ($this->environment === 'prod') {
            $baseDir = sprintf('/tmp/%s', $this->getCurrentTag());
        }

        return $baseDir . '/var/cache/' . $this->environment;
    }

    public function getLogDir()
    {
        $baseDir = $this->getProjectDir();
        if ($this->environment === 'prod') {
            $baseDir = sprintf('/tmp/%s', $this->getCurrentTag());
        }

        return $baseDir . '/var/log';
    }
    
    public function registerBundles()
    {
        $contents = require $this->getProjectDir().'/config/bundles.php';
        foreach ($contents as $class => $envs) {
            if ($envs[$this->environment] ?? $envs['all'] ?? false) {
                yield new $class();
            }
        }
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $container->addResource(new FileResource($this->getProjectDir().'/config/bundles.php'));
        $container->setParameter('container.dumper.inline_class_loader', true);
        $confDir = $this->getProjectDir().'/config';

        $loader->load($confDir.'/{packages}/*'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{packages}/'.$this->environment.'/**/*'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{services}'.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{services}_'.$this->environment.self::CONFIG_EXTS, 'glob');
        $loader->load($confDir.'/{services}/**/*'.self::CONFIG_EXTS, 'glob');
    }

    protected function configureRoutes(RouteCollectionBuilder $routes)
    {
        $confDir = $this->getProjectDir().'/config';

        $routes->import($confDir.'/{routes}/*'.self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir.'/{routes}/'.$this->environment.'/**/*'.self::CONFIG_EXTS, '/', 'glob');
        $routes->import($confDir.'/{routes}'.self::CONFIG_EXTS, '/', 'glob');
    }

    /**
     * @return string|null
     */
    private function getCurrentTag(): ?string
    {
        $tag = $_SERVER['SERVER_NAME'] ?? null;

        if (true === file_exists(__DIR__.'/../CURRENT_PRODUCTION_TAG')) {
            $tag = trim((string)file_get_contents(__DIR__.'/../CURRENT_PRODUCTION_TAG'));
        }
        if ('cli' === PHP_SAPI && 'production' === posix_getpwuid(posix_geteuid())['name']) {
            $tag .= 'DEPLOY'.time();
        }
        if (true === empty($tag)) {
            $tag = 'DEPLOY'.time();
        }

        return str_replace('/', '_', $tag);
    }
}
