<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190331100557 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fluent_log (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, rfid_reader_id SMALLINT UNSIGNED NOT NULL, path VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_65A61441985BA6D3 (rfid_reader_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_users (id INT AUTO_INCREMENT NOT NULL, rfid_reader_id INT DEFAULT NULL, status VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, middle_name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_5372078C985BA6D3 (rfid_reader_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE card_action_log (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, rfid_card_id INT NOT NULL, rfid_reader_id SMALLINT UNSIGNED NOT NULL, action_result TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_DEB76B8B7C1EDE59 (rfid_card_id), INDEX IDX_DEB76B8B985BA6D3 (rfid_reader_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rfid_card (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, card_uuid VARCHAR(64) NOT NULL, secret_data VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_2E800D6E3D4C5204 (card_uuid), UNIQUE INDEX UNIQ_2E800D6EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fluent_log ADD CONSTRAINT FK_65A61441985BA6D3 FOREIGN KEY (rfid_reader_id) REFERENCES rfid_reader (id)');
        $this->addSql('ALTER TABLE company_users ADD CONSTRAINT FK_5372078C985BA6D3 FOREIGN KEY (rfid_reader_id) REFERENCES rfid_card (id)');
        $this->addSql('ALTER TABLE card_action_log ADD CONSTRAINT FK_DEB76B8B7C1EDE59 FOREIGN KEY (rfid_card_id) REFERENCES rfid_card (id)');
        $this->addSql('ALTER TABLE card_action_log ADD CONSTRAINT FK_DEB76B8B985BA6D3 FOREIGN KEY (rfid_reader_id) REFERENCES rfid_reader (id)');
        $this->addSql('ALTER TABLE rfid_card ADD CONSTRAINT FK_2E800D6EA76ED395 FOREIGN KEY (user_id) REFERENCES company_users (id)');
        $this->addSql('DROP INDEX UNIQ_8D93D649D17F50A6 ON user');
        $this->addSql('ALTER TABLE user ADD login VARCHAR(64) NOT NULL, ADD name VARCHAR(255) NOT NULL, DROP uuid, DROP status, DROP first_name, DROP middle_name, DROP last_name, DROP gender, DROP phone, DROP birth_date');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649AA08CB10 ON user (login)');
        $this->addSql('DROP INDEX UNIQ_1D310696D17F50A6 ON rfid_reader');
        $this->addSql('ALTER TABLE rfid_reader CHANGE uuid reader_uuid VARCHAR(32) NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1D31069630A2B664 ON rfid_reader (reader_uuid)');
        $this->addSql('ALTER TABLE controller_device CHANGE last_sync_at last_ping_at DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rfid_card DROP FOREIGN KEY FK_2E800D6EA76ED395');
        $this->addSql('ALTER TABLE company_users DROP FOREIGN KEY FK_5372078C985BA6D3');
        $this->addSql('ALTER TABLE card_action_log DROP FOREIGN KEY FK_DEB76B8B7C1EDE59');
        $this->addSql('DROP TABLE fluent_log');
        $this->addSql('DROP TABLE company_users');
        $this->addSql('DROP TABLE card_action_log');
        $this->addSql('DROP TABLE rfid_card');
        $this->addSql('ALTER TABLE controller_device CHANGE last_ping_at last_sync_at DATETIME DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_1D31069630A2B664 ON rfid_reader');
        $this->addSql('ALTER TABLE rfid_reader CHANGE reader_uuid uuid VARCHAR(32) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1D310696D17F50A6 ON rfid_reader (uuid)');
        $this->addSql('DROP INDEX UNIQ_8D93D649AA08CB10 ON user');
        $this->addSql('ALTER TABLE user ADD uuid VARCHAR(180) NOT NULL COLLATE utf8mb4_unicode_ci, ADD first_name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD middle_name VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD last_name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD gender VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD phone VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, ADD birth_date DATE NOT NULL, DROP login, CHANGE name status VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649D17F50A6 ON user (uuid)');
    }
}
