<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190306093858 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rfid_reader (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, controller_device_id SMALLINT UNSIGNED DEFAULT NULL, uuid VARCHAR(32) NOT NULL, status VARCHAR(32) NOT NULL, description VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_1D310696D17F50A6 (uuid), INDEX IDX_1D3106961E2D8AFD (controller_device_id), INDEX status_idx (status), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE controller_device (id SMALLINT UNSIGNED AUTO_INCREMENT NOT NULL, uuid VARCHAR(32) NOT NULL, status VARCHAR(32) NOT NULL, location VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, last_sync_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_863F7080D17F50A6 (uuid), INDEX status_idx (status), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rfid_reader ADD CONSTRAINT FK_1D3106961E2D8AFD FOREIGN KEY (controller_device_id) REFERENCES controller_device (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rfid_reader DROP FOREIGN KEY FK_1D3106961E2D8AFD');
        $this->addSql('DROP TABLE rfid_reader');
        $this->addSql('DROP TABLE controller_device');
    }
}
