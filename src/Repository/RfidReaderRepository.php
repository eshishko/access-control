<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\RfidReader;

class RfidReaderRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RfidReader::class);
    }

    /**
     * @param RfidReader $rfidReader
     * @return RfidReader
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(RfidReader $rfidReader): RfidReader
    {
        $this->getEntityManager()->merge($rfidReader);
        $this->getEntityManager()->flush();

        return $rfidReader;
    }
}