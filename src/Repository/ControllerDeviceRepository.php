<?php

namespace App\Repository;

use App\Entity\ControllerDevice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class ControllerDeviceRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ControllerDevice::class);
    }

    /**
     * @param ControllerDevice $controllerDevice
     * @return ControllerDevice
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(ControllerDevice $controllerDevice): ControllerDevice
    {
        $this->getEntityManager()->merge($controllerDevice);
        $this->getEntityManager()->flush();

        return $controllerDevice;
    }
}