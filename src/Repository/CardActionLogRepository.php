<?php

namespace App\Repository;

use App\Entity\CardActionLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class CardActionLogRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CardActionLog::class);
    }

    /**
     * @param CardActionLog $log
     * @return CardActionLog
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create(CardActionLog $log): CardActionLog
    {
        $this->getEntityManager()->persist($log);
        $this->getEntityManager()->flush();

        return $log;
    }
}