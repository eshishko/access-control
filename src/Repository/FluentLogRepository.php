<?php

namespace App\Repository;

use App\Entity\FluentLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class FluentLogRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FluentLog::class);
    }

    /**
     * @param FluentLog $fluentLog
     * @return FluentLog
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(FluentLog $fluentLog): FluentLog
    {
        $this->getEntityManager()->persist($fluentLog);
        $this->getEntityManager()->flush();

        return $fluentLog;
    }
}